from django.contrib import admin
from django.urls import path
from account import views
from django.urls import include
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('aggregrator.urls')),
    path('',views.user_login,name="login"),
    path('register/',views.register,name="register"),
    path('logout/', views.user_logout, name='logout'),
]
