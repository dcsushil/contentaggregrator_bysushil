from django.shortcuts import render
from account.forms import UserForm

from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate,login,logout
from django.urls import reverse
from django.contrib.auth.decorators import login_required


def register(request):
    registered = False

    if request.method == "POST":
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():

            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True
        else:
            print(user_form.errors)
    else:
         user_form = UserForm()

    return render(request,'signup.html',
                 {'user_form':user_form,
                 'registered':registered})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('login'))



def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username,password=password )
        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('subscribe'))
            else:
                return HttpResponse("Account not active")
        else:
            print("Login failed")
            return HttpResponse("Invalid username or password !")
    else:
        return render(request,'login.html')
