from django.apps import AppConfig


class AggregratorConfig(AppConfig):
    name = 'aggregrator'
