from django import forms


Topics = [
    ('technology', 'Technology'),
    ('politics', 'Politics'),
    ('arts', 'Arts'),
    ('entertainment','Entertainment'),
    ('health','Health'),
    ('sports','Sports'),
    ('science','Science'),
    ('coronavirus','Coronavirus'),
    ('business','business')
]


class TopicForm(forms.Form):
    topics = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=Topics)
