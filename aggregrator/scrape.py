import feedparser
from newspaper import Article
from datetime import timedelta

from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.utils import timezone
from aggregrator import models


def fetch_news(news_cat):
    categories = {
        'TPS': 'top_stories_url',
        'ENT': 'entertainment_url',
        'BSN': 'business_url',
        'SPR': 'sports_url',
        'TCH': 'tech_url',
    }

    sources = models.NewsSourceModel.objects.values()
    news_links = []
    for source in sources:

        f = feedparser.parse(source[categories[news_cat]])
        MAX_LINKS = 5
        for i in range(MAX_LINKS):

            try:
                article = Article(f['entries'][i]['link'])
                article.download()
                article.parse()
                article.nlp()

                news_links.append(
                    {
                        'url': f['entries'][i]['link'],
                        'keywords': article.keywords,
                        'summary': article.summary,
                        'news_source_id': source['id'],
                        'title': f['entries'][i]['title'],
                    }
                )
            except:
                continue

    pop_indexes = []
    for i in range(len(news_links)):
        list1 = news_links[i]['keywords']

        if i <= (len(news_links) - 2):
            remaining_list = news_links[i+1:]
            for element in remaining_list:
                list2 = element['keywords']
                match_percentage = match_lists(list1, list2)

                if match_percentage >= 50:
                    pop_indexes.append(news_links.index(element))

    for pop_index in pop_indexes:
        news_links.pop(pop_index)

    q, _ = models.CategoryUrlsModel.objects.get_or_create(news_cat=news_cat)
    for news_link in news_links:
        q.urls.create(
            url=news_link['url'],
            news_source_id=news_link['news_source_id'],
            keywords=news_link['keywords'],
            summary=news_link['summary'],
            title=news_link['title'],
        )
    q.save()


def match_lists(list1, list2):
    match_count = 0

    for element in list1:
        if element in list2:
            match_count = match_count + 1

    match_percentage = int(round(((match_count * 100) / len(list1)), 0))
    return match_percentage
