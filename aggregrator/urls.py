from django.urls import path
from aggregrator import views


urlpatterns = [
    path('index/', views.home, name="Home"),
    path('next/', views.loadcontent, name="Loadcontent"),
    path('subscribe/',views.topics,name="subscribe"),
]
